import React from 'react'
import { Car } from '../interfaces'
import { makeStyles } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { useRouter } from 'next/router';

type Props = {
  data: Car
}

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: 10
  },
  rootMaintenance: {
    maxWidth: 345,
    margin: 10,
    background: '#db9e9e5c'
  },
  media: {
    height: 140,
  },
});


const ListItem = ({ data }: Props) => {
  const classes = useStyles();

  const router = useRouter();

  const refreshData = () => {
    router.replace(router.asPath);
  }

  async function toggleMaintenance(data: Car) {
    const carId = data.id;
    data.maintenance = !data.maintenance;
    
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}/cars/${carId}`, {
      method: 'PATCH',
      headers: {
      'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    });
    // // Check that our status code is in the 200s,
    // // meaning the request was successful.
    if (res.status < 300) {
      refreshData();
    }
  }

  return (
    <Card className={data.maintenance ? classes.rootMaintenance : classes.root}>
      <CardActionArea>
        <CardMedia
          className={classes.media}
          image={data.image}
          title={data.make}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {data.make} {data.model}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {data.description}
            <br/> KM: {data.km}
            <br/> Estimate Date: {data.estimateDate}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        <Button onClick={() => toggleMaintenance(data)} size="small" color="primary">
          {data.maintenance ? "Solve issue" : "Send to Maintenance"}
        </Button>
      </CardActions>
    </Card>
  )
}

export default ListItem
