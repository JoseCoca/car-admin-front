import React, { ReactNode } from 'react'
import Head from 'next/head'
import Navbar from './Navbar'
import { Box } from '@material-ui/core'


type Props = {
  children?: ReactNode
  title?: string
}


const Layout = ({ children, title = 'This is the default title' }: Props) => {
  return (
    <div>
      <Head>
        <title>{title}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Navbar></Navbar>
      <Box p={2} bgcolor="background.paper">
        {children}
      </Box>
    </div>
  )
}

export default Layout
