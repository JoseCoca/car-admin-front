import { Car } from '../interfaces'

/** Dummy car data. */
export const sampleCarData: Car[] = [
  { 
    id:3344,
    description:" change of pads ",
    make:" Nissan ",
    model:"Sentra",
    km:6000,
    image:"http://3.23.108.188/cars/sentra.jpg"
  },
  {
    id:32434,
    description:"Change ligths", 
    make:"Chevrolet",
    model:"Spark",
    km:16098,
    estimateDate: 'string',
    image:"http://3.23.108.188/cars/spark.jpg",
    maintenance: true,
  },
  {
    id:4234344,
    description:"Change Transmission (CVT)", 
    make:"Chevrolet",
    model:"Aveo NG",
    estimateDate:"2021/09/07",
    km:33460,
    image:"http://3.23.108.188/cars/aveo.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3345,
    image:"http://3.23.108.188/cars/vento.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3346,
    image:"http://3.23.108.188/cars/vento.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3347,
    image:"http://3.23.108.188/cars/vento.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3348,
    image:"http://3.23.108.188/cars/vento.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3349,
    image:"http://3.23.108.188/cars/vento.jpg"
  },
  {
    description:" change of pads ", 
    make:"Volkswagen",
    model:"Vento",
    km:80050,
    id:3350,
    image:"http://3.23.108.188/cars/vento.jpg"
  },

]
