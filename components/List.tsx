import * as React from 'react'
import ListItem from './ListItem'
import { Car } from '../interfaces'
import { Grid } from '@material-ui/core';

type Props = {
  items: Car[]
}

const List = ({ items }: Props) => (
  <div>
    <Grid  
      container
      direction="row"
      justify="center"
      alignItems="center" >
      {items.map((item) => (
        <div key={item.id}>
            <ListItem data={item} />
        </div>
      ))}
    </Grid>
  </div>
)

export default List
