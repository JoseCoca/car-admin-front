// You can include shared interfaces/types in a separate file
// and then use them in any component by importing them. For
// example, to import the interface below do:

export type Car = {
  id: number
  description: string
  make: string
  model: string
  estimateDate?: string
  image: string
  km?: number
  maintenance?: boolean
}
