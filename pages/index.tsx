import Layout from '../components/Layout'
import Link from 'next/link';

const IndexPage = () => (
  <Layout title="About | Next.js + TypeScript Example">
    <h1>About</h1>
    <p>This is a React app example based on specified requirements</p>
    <h2>Technologies used</h2>
    <ul>
      <li>Next.js (React Fullstack framework)</li>
      <li>Nest.js (Node Js Backend Framework)</li>
      <li>React</li>
      <li>Typescript (Within Next and Nest)</li>
      <li>Node</li>
      <li>Material UI components</li>
    </ul>
    <h2>Repositories</h2>
    <p>Frontend repo can be found  <a href="https://bitbucket.org/JoseCoca/car-admin-front/src/main/" target="_blank">here</a></p>
    <p>Backend repo can be found  <a href="https://bitbucket.org/JoseCoca/car-admin-back/src/master/" target="_blank">here</a></p>

    <h2>Deployment</h2>
    <p>Frontend deployment is based on vercel connected to the bitbucket repo</p>
    <p>Backend deployment is based on DigitalOcean with a Docker droplet, swagger API specification can be consulted <a href="http://143.110.229.110:3200/api" target="_blank">here</a></p>
    <p>Feel free to add as many data as you want through swagger api link</p>

    <h2>
      You can continue by looking at the cars list on the 
      <Link href="/cars"> next page</Link>
    </h2>

  </Layout>
)

export default IndexPage
