import * as React from 'react'

import { Car } from '../interfaces'

type ListDetailProps = {
  item: Car
}

const ListDetail = ({ item: car }: ListDetailProps) => (
  <div>
    <h1>Detail for {car.make} {car.model}</h1>
    <p>ID: {car.id}</p>
  </div>
)

export default ListDetail
