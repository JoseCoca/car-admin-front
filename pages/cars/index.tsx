import { GetStaticProps } from 'next'
import Link from 'next/link'
import { Car } from '../../interfaces'
// import { sampleCarData } from '../../utils/sample-data'
import Layout from '../../components/Layout'
import List from '../../components/List'

type Props = {
  items: Car[]
}

const WithStaticProps = ({ items }: Props) => (
  <Layout title="Cars List | Next.js">
    <h1>Cars List</h1>
    <List items={items} />
    <p>
      <Link href="/">
        <a>Go home</a>
      </Link>
    </p>
  </Layout>
)

export const getStaticProps: GetStaticProps = async () => {
  // Including static props in a Next.js function component page.
  const fetchItems = await fetch(`${process.env.NEXT_PUBLIC_API_ENDPOINT}/cars`);
  
  // uncomment this line to use sample data  
  // const items: Car[] = sampleCarData;
  const items: Car[] = await fetchItems.json();

  return { props: { items } }
}

export default WithStaticProps
